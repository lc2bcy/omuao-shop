package com.omuao.eureka.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Eureka 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@EnableEurekaServer
@SpringBootApplication
public class EurekaRunner {

    public static void main(String[] args) {
        SpringApplication.run(EurekaRunner.class, args);
    }

}
