package com.omuao.web.store.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * WEB商城 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@SpringBootApplication
@EnableFeignClients
public class WebStoreRunner {

    public static void main(String[] args) {
        SpringApplication.run(WebStoreRunner.class, args);
    }

}
