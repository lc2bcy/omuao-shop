package com.omuao.web.admin.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * WEB后台 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@SpringBootApplication
@EnableFeignClients
public class WebAdminRunner {

    public static void main(String[] args) {
        SpringApplication.run(WebAdminRunner.class, args);
    }

}
