# omuao-shop

#### 介绍

Omuao Shop是一套开源的网上商城，它采用了当下比较流行的SpringCloud构建相关的微服务。

#### 前端架构：

VUE2.x

####  后端架构：

SpringCloud2.x + Mybatis + Mysql + ElasticSearch

#### 部署架构

Nginx（静态缓存） + Eureka（服务发现） + Gateway/Zuul（网关,API分流） + Ribbon（客户端负载均衡） + Hystrix（服务降级，熔断） + Feign（服务调用)

其他说明见[架构说明-部署](http://#)

#### 程序架构：

Springboot + Mybatis + Mysql/MongoDB/ES/Redis

分布式事务：LCN

数据权限：data-scope

导入导出：mofum-msdom

其他说明见[架构说明-程序](http://#)

#### 程序分包说明

    └─omuao-shop
        ├─omuao-core-lang								        -核心包（Entity,Bean,DTO）
        ├─omuao-common-utils							        -公共工具包
        ├─omuao-app-admin								        -APP 后台管理
        ├─omuao-eureka									    -eureka发现服务
        ├─omuao-gateway									    -网关
        ├─omuao-service-cms								    -CMS服务
        ├─omuao-service-cms-contract					        -CMS交互协议
        ├─omuao-service-filesystem						    -文件服务
        ├─omuao-service-filesystem-contract				    -文件交互协议
        ├─omuao-service-market							    -营销服务
        ├─omuao-service-market-contract					    -营销交互协议
        ├─omuao-service-message							    -消息服务
        ├─omuao-service-message-contract				        -消息交互协议
        ├─omuao-service-order							        -订单服务
        ├─omuao-service-order-contract					    -订单交互协议
        ├─omuao-service-pay								    -支付服务
        ├─omuao-service-pay-contract					        -支付交互协议
        ├─omuao-service-product							    -商品服务
        ├─omuao-service-product-contract				        -商品交互协议
        ├─omuao-service-user							        -用户服务
        ├─omuao-service-user-contract					        -用户交互协议
        ├─omuao-service-wms								    -仓储服务
        ├─omuao-service-wms-contract					        -仓储交互协议
        ├─omuao-service-search							    -搜索服务
        ├─omuao-service-search-contract					    -搜索交互协议
        ├─omuao-service-scheduler							    -调度服务
        ├─omuao-service-scheduler-contract			        -调度交互协议
        ├─omuao-tx-manager								    -分布式事务
        ├─omuao-web-admin								        -WEB后台管理
        ├─omuao-web-mobile-store						        -WEB手机商城
        └─omuao-web-store								        -WEB商城（PC）
        
#### 程序调用说明

**端口约定**

eureka服务 端口区间取值：7000-9000（不含9000）

eureka应用 端口区间取值：9000以上

Nginx 公网端口：80

host:service-name

以下是service-name的对应Eureka 服务名称关系，需再driver/etc/host进行相关配置

    eureka              - 服务注册中心（发现服务）(8761 - 8999)
    gateway             - 微服务网关服务（9000-9100）
    cms                 - 内容管理系统（7900 - 7999）
    filesystem          - 文件管理系统（7700 - 7799）
    market              - 营销服务（7600 - 7699）
    message             - 消息服务（7800 - 7899）
    order               - 订单服务（7200 - 7299）
    pay                 - 支付服务（7300 - 7399）
    product             - 商品服务（7100-7199）
    user                - 用户服务（7400 - 7499）
    wms                 - 仓储管理系统（7500 - 7599）
    search              - 搜索服务（8000-8099）


**hosts文件**

    127.0.0.1 redis
    127.0.0.1 eureka
    127.0.0.1 tx-manager
    127.0.0.1 database



**例1：订单调用商品业务**

order-service -> product-contract -> product-service

**例2：应用请求调用商品业务**

web/app request -> product-contract -> product-service 

#### 业务模块：

1. 商品服务（端口P:7100 - 7199）
2. 订单服务（端口P:7200 - 7299）
3. 支付服务（端口P:7300 - 7399）
4. 用户服务（端口P:7400 - 7499）
5. 仓储服务（端口P:7500 - 7599）
6. 营销服务（端口P:7600 - 7699）
7. 文件服务（端口P:7700 - 7799）
8. 消息服务（端口P:7800 - 7899）
9. 内容服务（端口P:7900 - 7999）
10. 搜索服务（端口P:8000 - 8099）
11. 前端服务（端口P:9000 - 10000）



其他说明见[服务说明](http://#)

#### 编码规范：

见[编码规约说明](http://#)

#### 人员配备：

##### 后端开发人名单：

Tan             （搜索业务）

Loops           （用户业务）

huangh           (消息业务)

緣亻分d兲空     （订单业务）

abc582915847    （编码标准制定/代码质量监督）

Lily            （仓库业务）

玉米            （商品业务）

Shanks          （暂定）

鬼迹            （暂定）

煊赫            （暂定）


##### 前端开发人员名单：

dlyndon     （前端业务）

阿柒柒      （前端业务）

##### 鸣谢名单

#### 使用本商城的公司名单


