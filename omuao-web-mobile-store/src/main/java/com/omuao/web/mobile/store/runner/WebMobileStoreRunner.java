package com.omuao.web.mobile.store.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * WEB手机商城 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@SpringBootApplication
@EnableFeignClients
public class WebMobileStoreRunner {

    public static void main(String[] args) {
        SpringApplication.run(WebMobileStoreRunner.class, args);
    }

}
