package com.omuao.common.utils.http;

/**
 * 返回数据结果
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
public interface DataResult<T> extends Result<T> {

    /**
     * 获得数据
     *
     * @return
     */
    T getData();

    /**
     * 设置数据
     *
     * @param data 数据
     */
    void setData(T data);

}
