package com.omuao.common.utils.http;

import java.util.List;

public interface ContentResult<T> extends Result<T>, RowsResult<T> {

    /**
     * 获得头部数据
     *
     * @return 数据
     */
    List<T> getHeaderData();

    /**
     * 设置头部数据
     *
     * @param rows 数据
     */
    void setHeaderData(List<T> rows);

    /**
     * 获得底部数据
     *
     * @return
     */
    List<T> getFooterData();

    /**
     * 设置底部数据
     *
     * @param rows 数据
     */
    void setFooterData(List<T> rows);

}
