package com.omuao.common.utils.http;

import java.util.List;

public interface RowsResult<T> extends Result<T> {

    /**
     * 获得主体数据
     *
     * @return
     */
    List<T> getBodyData();

    /**
     * 设置主体数据
     *
     * @param rows 数据
     */
    void setBodyData(List<T> rows);

    /**
     * 获得主体部分数据数量
     *
     * @return 总数
     */
    long getTotal();

    /**
     * 设置主体部分数据数量
     *
     * @param total 数据数量
     */
    void setTotal(long total);

    /**
     * 获得主体部分数据的分页数
     *
     * @return
     */
    long getTotalPages();

    /**
     * 设置主体部分数据的分页数
     *
     * @param totalPages 分页数
     */
    void setTotalPages(long totalPages);
}
