package com.omuao.common.utils.http;

import java.util.List;

/**
 * 返回结果
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
public interface Result<T> {

    /**
     * 状态码
     *
     * @return
     */
    int getStatus();

    /**
     * 设置状态码
     *
     * @param status 状态码
     */
    void setStatus(int status);

    /**
     * 编码
     *
     * @return
     */
    int getCode();

    /**
     * 设置错误码
     *
     * @param code 错误码
     */
    void setCode(int code);

    /**
     * 获得消息
     *
     * @return
     */
    String getMessage();

    /**
     * 设置消息
     *
     * @param message 消息
     */
    void setMessage(String message);

}
