package com.omuao.common.utils.idgen;

import org.junit.Test;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import static org.junit.Assert.assertEquals;


public class IDGenTest {

    @Test
    public void test() throws InterruptedException {

        Sequence sequence = new Sequence(0, true, false);

        Set<Long> set = new CopyOnWriteArraySet<>();

        for (int i = 0; i < 10000; i++) {

            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    long id = sequence.nextId();
                    set.add(id);
                }
            });

            t.start();
        }
        //获取正确结果
        Thread.sleep(10000l);
        assertEquals(set.size(),10000);

    }

}
