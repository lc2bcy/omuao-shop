package com.omuao.common.utils.http;

import com.omuao.common.utils.http.impl.ResultImpl;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * 测试返回结果
 */
public class ResultTest {

    @Test
    public void test() {

        Result result = ResultImpl.success("test", null);
        println(result);
        assertEquals(result.getClass().getSimpleName(), "MessageResultImpl");

        result = ResultImpl.success("test", "test");
        println(result);
        assertEquals(result.getClass().getSimpleName(), "SingleResultImpl");

        result = ResultImpl.success("test", "test", new ArrayList<>(), 1, 1);
        println(result);
        assertEquals(result.getClass().getSimpleName(), "ResultImpl");

        result = ResultImpl.success("test", null, new ArrayList<>(), 0, 0);
        println(result);
        assertEquals(result.getClass().getSimpleName(), "RowsResultImpl");

        result = ResultImpl.success("test", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), 0, 0);
        println(result);
        assertEquals(result.getClass().getSimpleName(), "RowsResultExtImpl");

        result = ResultImpl.failed("failed");
        println(result);
        assertEquals(result.getClass().getSimpleName(), "ErrorResultImpl");
    }

    public static void println(Result result) {
        System.out.println(result);
    }
}
