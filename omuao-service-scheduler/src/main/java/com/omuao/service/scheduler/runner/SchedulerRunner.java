package com.omuao.service.scheduler.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 定时调度 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class SchedulerRunner {

    public static void main(String[] args) {
        SpringApplication.run(SchedulerRunner.class, args);
    }

}
