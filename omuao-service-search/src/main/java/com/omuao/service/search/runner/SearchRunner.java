package com.omuao.service.search.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * Gateway 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class SearchRunner {

    public static void main(String[] args) {
        SpringApplication.run(SearchRunner.class, args);
    }

}
