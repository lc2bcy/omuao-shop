package com.omuao.service.message.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 消息 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class MessageRunner {

    public static void main(String[] args) {
        SpringApplication.run(MessageRunner.class, args);
    }

}
