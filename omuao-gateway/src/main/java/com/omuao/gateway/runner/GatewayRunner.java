package com.omuao.gateway.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Gateway 启动类
 *
 * @author yumi@omuao.com
 * @since 2019-10-11
 **/
@SpringBootApplication
public class GatewayRunner {

    public static void main(String[] args) {
        SpringApplication.run(GatewayRunner.class, args);
    }

}
